<?php
	session_start();
	$p = array_map('trim', $_POST);
	$valide = true;
	$erreurs = [];

	if (strlen($p['nom']) < 3) {
		$valide = false;
		$erreurs[] = 'Le nom n\'est pas assez long';
	}
	if (strlen($p['prenom']) < 3) {
		$valide = false;
		$erreurs[] = 'Le prénom n\'est pas assez long';
	}
	if (preg_match("#^[a-z0-9_.-]+@[a-z0-9-]{2,}\.[a-z]{2,}$#i", $p['email']) === 0) {
		$valide = false;
		$erreurs[] = "L'adresse mail n'a pas un format valide";
	}
	if (strlen($p['message']) < 20 || strpos($p['message'], ' ') === false) {
		$valide = false;
		$erreurs[] = "Le message n'est pas valide, il doit contenir au moins 20 caractères et être composé de plusieurs mots";
	}

	

	if (!$valide) {
		$_SESSION['erreurs'] = $erreurs;
		$_SESSION['old_post'] = $_POST;
		header("Location: contact.php");
		exit;
	}

	// redirection vers les portfolios
	$dests = [
		'jeanp' => 'http://jeanp.dijon.codeur.online/enregistrer_message.php',
		'marie-pierrel' => 'http://marie-pierrel.dijon.codeur.online/Comebackkids/',
		'quentinp' => 'http://quentinp.dijon.codeur.online/PHPExo1/simon.php'
	];

	$dest = $dests[$_POST['dest']];
	unset($_POST['dest']);
	$get = [];
	foreach ($_POST as $key => $value) {
		$get[] = "$key=".urlencode($value);
	}
	$get = "?".implode('&', $get);

	header('Location: '.$dest.$get);
?>