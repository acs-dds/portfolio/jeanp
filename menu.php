<ul class="menu">
	<li><span>Jean</span>
		<ul class="submenu">
			<li><a href="http://jeanp.dijon.codeur.online/simon.php">Simon</a></li>
			<li><a href="http://jeanp.dijon.codeur.online/crc.php">CRC</a></li>
			<li><a href="http://jeanp.dijon.codeur.online/carrousel/carrousel.php">Carrousel</a></li>
		</ul>
	</li>
	<li><span>Marie-Pierre</span>
		<ul class="submenu">
			<li><a href="http://marie-pierrel.dijon.codeur.online/SimonMp/Simonv2.php">Simon</a></li>
			<li><a href="http://marie-pierrel.dijon.codeur.online/Comebackkids/">Site CBK</a></li>
			<li><a href="http://marie-pierrel.dijon.codeur.online/Calculette/calculette.html">Calculette</a></li>
		</ul>
	</li>
	<li><a href="http://jeanp.dijon.codeur.online/contact.php">Contact</a></li>
</ul>
<style type="text/css">
	ul.menu, ul.submenu {
		display: block;
		border-bottom: 1px solid #54c58d;
		padding: 0 0 15px;
	}
	ul.menu > li {
		display: inline-block;
		margin: 4px 12px;
	}
	ul.menu > li > ul {
		display: none;
	    position: absolute;
	    background-color: #fff;
	}
	ul.menu > li:hover > ul {
		display: block;
	}
	ul.menu > li > ul > li {
		display: block;
	}
</style>