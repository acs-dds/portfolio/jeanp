<?php

require_once __DIR__.'/laipisseummapper.php';

class LaipisseumController {
	private $mapper;

	public function __construct() {
		$this->mapper = new LaipisseumMapper();
	}

	public function genererParagrapheAction() {
		return $this->mapper->genererParagraphe();
	}

	public function genererParagraphesAction($nb, $themes = []) {
		foreach ($themes as $nomDeTheme) {
			$this->mapper->loadTheme($nomDeTheme);
		}
		return $this->mapper->genererParagraphes($nb);
	}
}