<?php

class Theme {
	private $mots;
	private $nom;
	private $auteur;
	private $hex;
	private $fichier;

	public function __construct($mots, $nom, $auteur, $hex, $fichier) {
		foreach ($mots as &$mot) {
			$mot = '<span style="color: #'.$hex.'">'.$mot.'</span>';
		}
		$this->mots = $mots;
		$this->nom = $nom;
		$this->auteur = $auteur;
		$this->hex = $hex;
		$this->fichier = $fichier;
	}

	public function renderCheckbox() {
		return "<input type=\"checkbox\" name=\"themes[]\" value=\"{$this->fichier}\" id=\"{$this->fichier}\"><label for=\"{$this->fichier}\">{$this->nom} by <em>{$this->auteur}</em></label>";
	}

	public function getMots() {
		return $this->mots;
	}
}