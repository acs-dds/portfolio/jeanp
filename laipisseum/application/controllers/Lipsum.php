<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lipsum extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// generer mon para via mapper
		$this->load->model('lipsum_model');

		$data['para'] = $this->lipsum_model->genererParagraphe();

		$this->load->view('common/header');
		$this->load->view('paragraphe', $data);
		$this->load->view('common/footer');
	}

	public function generer() {

		$nb = $this->input->post('nb');
		$themes = $this->input->post('themes');


		$this->load->model('lipsum_model');

		foreach ($themes as $theme) {
			$this->lipsum_model->loadTheme($theme);
		}

		$data['paras'] = $this->lipsum_model->genererParagraphes($nb);

		$this->load->view('common/header');
		$this->load->view('paragraphes', $data);
		$this->load->view('common/footer');
	}
}
