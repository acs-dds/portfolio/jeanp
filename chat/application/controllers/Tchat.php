<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tchat extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//$this->load->library('access'); // autoloadé
		$this->access->restrict();
		$this->load->model('message_model');
		$this->load->model('utilisateur_model');
		$this->utilisateur_model->ping($this->session->utilisateur['id']);
		if (!$this->session->has_userdata('discussion')) $this->session->discussion = 1;
	}

	public function index() {
		$data = ['messages' => $this->message_model->fetchMessages($this->session->discussion)];
		$this->session->lastFetch = time();
		$this->load->view('common/header');
		$this->load->view('chatbox', $data);
		$this->load->view('common/footer');
	}

	public function refresh() {
		$messages = $this->message_model->fetchMessagesSince($this->session->discussion, $this->session->lastFetch);
		$this->session->lastFetch = microtime(true);
		echo json_encode($messages);
	}

	public function post() {
		$this->message_model->saveMessage($this->input->post('message'), $this->session->utilisateur['id'], $this->session->discussion);
		echo 0;
	}

	public function getUsers() {
		echo json_encode($this->utilisateur_model->getUtilisateursEnLigne());
	}
}
