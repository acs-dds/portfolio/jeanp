<?php

class Utilisateur_model extends CI_Model {

        public function fetchUtilisateur($login, $mdp) {
                $req = "SELECT id, nom, prenom, pseudo FROM utilisateur WHERE pseudo = ? AND motdepasse = ?;";
        	return $this->db->query($req, [$login, md5($mdp)])->row_array();
        }

        public function saveUtilisateur($login, $mdp, $nom, $prenom) {
        	$id = $this->db->query(
        		"INSERT INTO utilisateur (nom, prenom, pseudo, motdepasse)
        		VALUES (?,?,?,?) RETURNING id;",
        		[$nom, $prenom, $login, md5($mdp)]
        	)->row()->id;
        	return ['id' => $id, 'pseudo' => $login, 'nom' => $nom, 'prenom' => $prenom];
        }

        public function ping($idutilisateur) {
                $this->db->query("UPDATE utilisateur SET ping = now() WHERE id = ?", [$idutilisateur]);
        }

        public function getUtilisateursEnLigne() {
                return $this->db->query("SELECT pseudo FROM utilisateur WHERE ping > now() - '1 minute'::interval;")->result_array();
        }
}