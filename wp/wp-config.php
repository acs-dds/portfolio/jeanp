<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'jeanp');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'jeanp');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'accesscouscous');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uKU~juey+B*LvfL,A@aM?R0^n~E51L+$G(BbKMlUv|Q{1d+Ch Vh I{{q/48~&n6');
define('SECURE_AUTH_KEY',  'zzzdOO0S(d@,l:eV;-SNZ~7qkt,H1S.MA= *A]C_n5.%S,zA_gFb.0> l,e#]y1V');
define('LOGGED_IN_KEY',    ':+<lz|e=tpc.)<c|7?/#?u;U$YHRDHM:Xle|(4q3!T%;pD%{Skg<5a<fleNX*#7[');
define('NONCE_KEY',        'B]t#&dk|KS`tvbqZdRY9^h&p(+m;CYP){2R+;TmmF:vAfCN-|eK%{ohp#]jz1u;(');
define('AUTH_SALT',        'pp{t34&N?iDwjW]+fc-52P&z$*`qtr_qhYJ{ 2AF?hKR~`=Ce@i%_1<%0p vk;Mj');
define('SECURE_AUTH_SALT', '!So1i+BYM0=p2[7~XVM)(<IP(0_Bm:m6SV:(0(;bVDJqC?&G_=C-^teMTk^yd`jC');
define('LOGGED_IN_SALT',   'Wk~C$bK>w4U:ipD/(, [!3mQa+tWgO^YZ0Yp2 8Hz3rOcU{xOD$4l+2ila;.1/%f');
define('NONCE_SALT',       '%{1p]v^[?hQpE7DM{ .XSv >84O(:q[-1OC GSxZ<>z4.z,Pr=JJf>cf*(EL/@H!');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'Qa8CmQLT_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');