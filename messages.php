<!DOCTYPE html>
<html>
<head>
	<title>Messages</title>
</head>
<body>
<?php require 'menu.php'; ?>
<table border="1">
	<thead>
		<tr>
			<th>Nom</th>
			<th>Prénom</th>
			<th>Message</th>
			<th>Date</th>
		</tr>
	</thead>
	<?php
		require '/home/jeanp/xdb.php';
		$messages = Xdb::get();
		foreach ($messages as $message): ?>
	<tr>
		<td><?php echo $message['nom']; ?></td>
		<td><?php echo $message['prenom']; ?></td>
		<td><?php echo $message['message']; ?></td>
		<td><?php
				setlocale(LC_TIME,"fr_FR");
				echo ucfirst(strftime("%A %e %B %Y à %k:%M:%S", $message['date']));
			?></td>
	</tr>
	<?php endforeach; ?>
</table>
</body>
</html>