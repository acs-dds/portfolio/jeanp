<?php
$menu = [
	'accueil' => 'accueil.php',
	'nos avantages' => 'avantages.php',
	'nos services' => [
		'lustrage' => 'services/lustrage.php',
		'decapage' => 'services/decapage.php',
		'poncage'  => 'services/poncage.php',
		'vernissage' => 'services/vernissage.php'
	],
	'prendre rdv' => 'rdv.php',
	'nous contacter' => 'contact.php',
	'extranet' => 'extranet.php'
];