<?php

header('Content-Type:text/plain');

try {
	$pdo = new PDO("pgsql:host=localhost;dbname=tests_et_exos;user=test;password=123");
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
    exit;
}

$test = $pdo->query("SELECT * FROM vueapprnt");
if (!$test) {
	print_r($pdo->errorInfo());
	exit;
}
$result = $test->fetchAll(PDO::FETCH_ASSOC);

foreach ($result as $apprenant) {
	print_r($apprenant);
	echo PHP_EOL.PHP_EOL;
}