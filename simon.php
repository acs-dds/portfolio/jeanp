<!doctype html>

<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Jeu du Simon</title>
  <style>
  	body {
  		background-color: #d83b43;
  		font-family: Verdana, sans-serif;
  	}
  	button:focus {
  		outline: none;
  	}
  	article#simon {
  		margin: auto;
  		padding: 5px;
  		width: 800px;
  		min-height: 500px;
  		background-color: #efefef;
  	}
  	main.jeu {
  		font-size: 0;
  		width: 240px;
  		margin: 35px auto;
  	}
  	main.jeu > h1 {
  		font-size: 25px;
  		text-align: center;
  		margin: .8em -70px 2em;
  	}
  	main.jeu.large {
  		width: 360px;
  	}
  	button.btn {
  		display: inline-block;
  		padding: 0;
  		width: 120px;
  		height: 120px;
  		border: none;
  	}
  	span.push {
  		width: inherit;
  		height: inherit;
  		border-radius: inherit;
  		background-color: rgba(255, 255, 255, .8);
  		display: none;
  	}
  	.btn:active > span.push, .btn.pushed > span.push {
  		display: inline-block;
  	}
  	.btn#btn1 {
  		border-top-left-radius: 100%;
  	}
  	.btn#btn2 {
  		border-top-right-radius: 100%;
  	}
  	.btn#btn3 {
  		border-bottom-left-radius: 100%;
  	}
  	.btn#btn4 {
  		border-bottom-right-radius: 100%;
  	}
  	button.btn.blue {
  		background-color: #1f1fee;
  	}
  	button.btn.red {
  		background-color: #ee1f1f;
  	}
  	button.btn.green {
  		background-color: #1fee1f;
  	}
  	button.btn.yellow {
  		background-color: #eeee1f;
  	}
  	button.btn.pink {
  		background-color: #ee1faa;
  	}
  	button.btn.cyan {
  		background-color: #1feeee;
  	}
  	button.btn.orange {
  		background-color: #eeaa1f;
  	}
  	button.btn.purple {
  		background-color: #aa1fee;
  	}
  	button.btn.skyblue {
  		background-color: #1f99ee;
  	}
  	button.btn.hidden {
  		display: none;
  	}
  	output#message {
		height: 2em;
		line-height: 2em;
		display: block;
		text-align: center;
		font-size: 22px;
  	}
  	footer#lanceur {
	    text-align: center;
	    padding: 35px;
	}

	footer#lanceur > button {
	    border: none;
	    padding: 7px;
	    font-size: 1.1em;
	    margin: 0 8px;
	    border-radius: 5px;
	}
  </style>
</head>

<body>
	<?php require 'menu.php'; ?>
	<section class="boutons">
	</section>
	<article id="simon">
		<main class="jeu" id="jeu">
			<h1>Simon, un jeu trop rigolo</h1>
			<button class="btn red" id="btn1"><span class="push"></span></button>
			<button class="btn cyan med hidden" id="btn5"><span class="push"></span></button>
			<button class="btn yellow" id="btn2"><span class="push"></span></button>
			<button class="btn skyblue hard hidden" id="btn7"><span class="push"></span></button>
			<button class="btn orange hard hidden" id="btn8"><span class="push"></span></button>
			<button class="btn purple hard hidden" id="btn9"><span class="push"></span></button>
			<button class="btn green" id="btn3"><span class="push"></span></button>
			<button class="btn pink med hidden" id="btn6"><span class="push"></span></button>
			<button class="btn blue" id="btn4"><span class="push"></span></button>
		</main>
		<output id="message"></output>
		<footer id="lanceur">
			<button id="easy">Lancer une partie facile</button>
			<button id="med">Lancer une partie moyenne</button>
			<button id="hard">Lancer une partie difficile</button>
		</footer>
	</article>

  	<script>
  		for (var i = 0; i < document.getElementById('lanceur').children.length; i++) {
  			document.getElementById('lanceur').children[i].addEventListener("click", function() {
  				simon(this.id);
  			});
  		}

		var simon = function(diff) {
			// désactivons déjà les boutons de lancement
			for (var i = 0; i < document.getElementById('lanceur').children.length; i++) {
	  			document.getElementById('lanceur').children[i].disabled = true;
  			}

			// objet des difficultés
			var modes = {
				'easy': 4,
				'med' : 6,
				'hard': 9
			};

			var btsmed = document.getElementsByClassName("med"),
				btshard = document.getElementsByClassName("hard"),
				main = document.getElementById("jeu"),
				msg = document.getElementById("message");

			var mode = modes[diff];

			if (diff == 'easy') {
				// zone de jeu étroite
				main.classList.remove("large");

				// boutons med cachés
				for (var i = 0; i < btsmed.length; i++) {
					btsmed[i].classList.add("hidden");
				}

				// boutons hard cachés
				for (var i = 0; i < btshard.length; i++) {
					btshard[i].classList.add("hidden");
				}
			} else {
				// zone de jeu large
				main.classList.add("large");

				// boutons med affichés
				for (var i = 0; i < btsmed.length; i++) {
					btsmed[i].classList.remove("hidden");
				}

				if (diff == 'hard') {
					for (var i = 0; i < btshard.length; i++) {
						btshard[i].classList.remove("hidden");
					}
				}
			}

			// tableau des boutons
			var bt = [
				document.getElementById('btn1'),
				document.getElementById('btn2'),
				document.getElementById('btn3'),
				document.getElementById('btn4'),
				document.getElementById('btn5'),
				document.getElementById('btn6'),
				document.getElementById('btn7'),
				document.getElementById('btn8'),
				document.getElementById('btn9')
			];

			// la sequence des boutons
			var sequence = [];

			// l'index de la séquence
			var curr = 0;

			// le timer : au bout de 5 secondes sans cliquer sur un bouton, gameover
			var goTo;

			function displayMessage(message) {
				msg.innerText = message;
			}

			// démarre une séquence de longueur len
			function initSequence(len) {
				for (var i = 0; i < len; i++) {
					sequence.push(bt[Math.floor(Math.random() * mode)]);
				}
			}

			// joue la séquence
			// @param addBt bool : ajoute un bouton au préalable si vrai
			function playSequence(addBt) {
				if (addBt) {
					sequence.push(bt[Math.floor(Math.random() * mode)]);
				}

				displayMessage("Mémorise la séquence");
				// lance le premier bouton
				playButton(0);
			}

			// active le bouton d'index curr dans la séquence
			function playButton(curr) {

				function turnOff(btn) {
					btn.classList.remove("pushed");
				}

				// s'il existe un bouton d'index curr dans la séquence
				if (sequence[curr]) {
					sequence[curr].classList.add("pushed"); // allume
					setTimeout(turnOff, 800, sequence[curr]); // éteint au bout de 800ms
					setTimeout(playButton, 1000, curr + 1); // passe au suivant au bout d'une seconde
				} else { // sinon c'est au tour de l'utilisateur de jouer
					userTurn();
				}
			}

			function gameOver(wrong) {
				if (wrong) {
					displayMessage("Mauvaise réponse ! Merci d'avoir joué !");
				} else {
					displayMessage("Temps écoulé ! Merci d'avoir joué !");
				}
				for (var i = 0; i < mode; i++) {
					bt[i].removeEventListener("click", badAnswer);
					bt[i].removeEventListener("click", goodAnswer);
				}
			}

			// gameover
			function badAnswer() {
				gameOver(true);
				// retire le timer du gameover quand le temps s'est écoulé
				clearTimeout(goTo);
			}

			// bravo, bouton suivant !
			function goodAnswer() {
				// on reset le timer
				clearTimeout(goTo);
				goTo = setTimeout(gameOver, 5000);

				// on remet le listener mauvaise réponse (temporaire)
				sequence[curr].addEventListener("click", badAnswer);
				// on retire le listener bonne réponse (temporaire)
				sequence[curr].removeEventListener("click", goodAnswer);
				curr++;
				if (curr == sequence.length) {
					displayMessage("Impec ! Continuons !");
					clearTimeout(goTo);
					for (var i = 0; i < mode; i++) {
						bt[i].removeEventListener("click", badAnswer);
					}
					setTimeout(playSequence, 500, true);
				} else {
					sequence[curr].removeEventListener("click", badAnswer);
					sequence[curr].addEventListener("click", goodAnswer);
				}
			}

			// lance le tour de l'utilisateur
			function userTurn() {
				// on lance le timer et on réinitialise l'élément courant de la séquence à 0
				goTo = setTimeout(gameOver, 5000);
				curr = 0;

				// cliquer sur n'importe quel bouton entraîne le gameover (temporaire)
				for (var i = 0; i < mode; i++) {
					bt[i].addEventListener("click", badAnswer);
				}

				// on retire le gameover du premier bouton de la séquence...
				sequence[0].removeEventListener("click", badAnswer);
				// et on déclenche au clic la fonction qui passera au prochain
				sequence[0].addEventListener("click", goodAnswer);

				displayMessage("À toi de jouer");
			}

			initSequence(4);
			displayMessage("Prêt ?");
			setTimeout(playSequence, 1500, false);

		};
  	</script>
</body>
</html>