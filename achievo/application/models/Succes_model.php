<?php

class Succes_model extends CI_Model {

        const FROUT_OK = 0;
        const FROUT_NOK = 1;

	private $identreprise;

	public function setIdEntreprise($identreprise) {
		$this->identreprise = $identreprise;
	}

	private function ok() {
		return isset($this->identreprise);
	}

        public function fetchAllSucces() {
        	if ($this->ok())
                	return $this->db->query("SELECT id, titre, intitule, objectif, progression, fini FROM vuesucces WHERE idutilisateur = ? ORDER BY id ASC", [$this->identreprise])->result();
        }

        public function fetchSucces($id) {
        	if ($this->ok())
                	return $this->db->query("SELECT id, titre, intitule, objectif, progression, fini FROM vuesucces WHERE idutilisateur = ? AND id = ?", [$this->identreprise, $id])->row();
        }

        public function updateSucces($id, $progression) {
                if ($this->ok()) {

                        if ($this->db->query("UPDATE succes SET progression = ? WHERE id = ? AND idutilisateur = ?;", [$progression, $id, $this->identreprise])) {
                                // ca a marché
                                return self::FROUT_OK;
                        } else {
                                // ca a pas marché
                                return self::FROUT_NOK;
                        }
                }
        }
}