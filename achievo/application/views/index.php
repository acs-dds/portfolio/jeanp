<div class="achievo-list">
	<?php foreach ($succes as $s): ?>
	<a class="achievo-link" href="<?php if ($s->id > 0): echo site_url('succes/'.url_title($s->titre, '-', true).'-'.$s->id); else: echo '#'; endif; ?>">
		<article class="achievo-achievement<?php if ($s->id == 0): ?> achievo-master<?php endif; ?>">
			<figure><?php if ($s->fini): ?>
				<i class="fa fa-trophy" aria-hidden="true"></i>
			<?php else: ?>
				<i class="fa fa-lock" aria-hidden="true"></i>
			<?php endif; ?></figure>
			<h2><?php echo $s->titre; ?></h2>
			<progress data-toggle="tooltip" data-placement="bottom" title="<?php echo htmlspecialchars($s->intitule, ENT_COMPAT).' → '.$s->progression.'/'.$s->objectif; ?>" value="<?php echo $s->progression; ?>" max="<?php echo $s->objectif; ?>"></progress>
		</article>
	</a>
	<?php endforeach; ?>
</div>