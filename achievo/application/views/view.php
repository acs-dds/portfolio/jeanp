<div class="achievo-view">
	<article class="achievo-achievement achievo-achievement-lg<?php if ($succes->fini): ?> achievo-fini<?php endif; ?>">
		<div class="achievo-achievement-head">
			<figure>
				<i class="fa fa-trophy" aria-hidden="true"></i>
				<i class="fa fa-lock" aria-hidden="true"></i>
			</figure>
			<div class="achievo-achievement-text">
				<h2><?php echo $succes->titre; ?></h2>
				<p class="achievo-intitule"><?php echo $succes->intitule; ?></p>
			</div>
		</div>
		<div class="achievo-achievement-progress">
			<label for="achievo-progress-input">Progression&nbsp;:&nbsp;</label>
			<input type="number" value="<?php echo $succes->progression; ?>" min="0" max="<?php echo $succes->objectif; ?>" id="achievo-progress-input"/>
			<progress id="achievo-progress-bar" value="<?php echo $succes->progression; ?>" max="<?php echo $succes->objectif; ?>"></progress>
		</div>
	</article>
	<div class="achievo-back"><a href="<?php echo base_url(); ?>"><i class="fa fa-chevron-left" aria-hidden="true"></i><p>Retourner à la liste</p></a></div>
</div>