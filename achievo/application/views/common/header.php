<!DOCTYPE html>
<html>
<head>
	<title>Achievo</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/font-awesome.min.css'); ?>">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,900|Pacifico" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css'); ?>">
</head>
<body>
<nav class="navbar navbar-default">
	<h1 class="navbar-brand"><i class="fa fa-check-square-o" aria-hidden="true"></i> Achievo</h1>
	<?php if (isset($utilisateur)): ?>
	<nav class="user-bar">@<?php echo $utilisateur; ?><br/><a class="logout-link" href="<?php echo site_url('signout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a></nav>
	<?php endif; ?>
</nav>
<div class="achievo-container container-fluid">
