		</div>
		<script src="<?php echo base_url('/scripts/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('/scripts/bootstrap.min.js'); ?>"></script>
		<script type="text/javascript">
			$(document).ready(function () {
				$('[data-toggle="tooltip"]').tooltip();

				$("#achievo-progress-input").change(function() {
					$("#achievo-progress-bar").val($(this).val());

					if ($(this).val() == $(this).prop('max')) {
						$(".achievo-achievement-lg").addClass("achievo-fini");
					} else {

						$(".achievo-achievement-lg").removeClass("achievo-fini");
					}

					$.post(
						'<?php echo site_url('achievo/update/'.$succes->id); ?>',
						{progression: $(this).val()},
						function (msg) {
							if (msg) {
								alert(msg);
							}
						}
					);
				});
			});
		</script>
	</body>
</html>