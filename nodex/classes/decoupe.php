<?php

class Commande {
	private $produit;
	private $epaisseur;
	private $client;
	private $decoupes = [];

	public function __construct($produit, $epaisseur, $client, $decoupes_srlz) {
		$this->produit = $produit;
		$this->epaisseur = $epaisseur;
		$this->client = $client;
		foreach ($decoupes_srlz as $decoupe) {
			$this->decoupes[] = new Decoupe($decoupe);
		}
	}

	public function renderHtml() {
		$html = "<section class=\"commande\">".$this->produit->renderHtml($this->epaisseur)."<ul class=\"decoupes-liste\">";
		foreach ($this->decoupes as $d) {
			$html .= "<li>".$d->renderHtml()."</li>";
		}
		return $html."</ul></section>";
	}
}

class Decoupe {
	private $gache = false;
	private $type;
	private $volume; // en cm3
	public static const RECTANGLE = 0;
	public static const CERCLE = 1;
	public static const CHEMIN = 2;

	public function __construct($chaine) {
		$coords = explode(",",substr($chaine, 5, -1));
		switch (substr($chaine, 1, 4)) {
			case 'rect':
				$this->type = self::RECTANGLE;
				$this->volume = $coords[2] * $coords[3] * $epaisseur / 1000;
				break;
			case 'circ':
				$this->type = self::CERCLE;
				$this->volume = $coords[2] * $coords[2] * M_PI * $epaisseur / 1000;
				break;
		}

		$this->gache = substr($chaine, 0, 1) == "-";
	}

	public function getVolume() {
		return $this->volume;
	}

	public function isGache() {
		return $this->gache;
	}

	public function renderHtml() {
		$html = "<article class=\"decoupe\">";
		if ($this->gache) $html .= "Gâche ";
		else $html .= "Forme ";
		switch ($this->type) {
			case self::RECTANGLE:
				$html .= "rectangulaire ";
				break;
			case self::CERCLE:
				$html .= "circulaire ";
				break;
			case self::CHEMIN:
				$html .= "libre ";
				break;
		}
		$html .= "de volume total <output>".$this->volume."</output> cm³</article>";
	}
}