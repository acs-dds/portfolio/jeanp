<?php
	if (!isset($_GET['ref']) || !isset($_GET['ep'])) {
		header('Location: accueil.php');
		exit;
	}

	require 'classes/mapper.php';
	session_start();

	$produit = $_SESSION['produit'] = Mapper::getProduit($_GET['ref']);

	$lo = @$_GET['lo']?:$produit->getLongueur();
	$la = @$_GET['la']?:$produit->getLargeur();


	if ((intval($lo) % 50) || (intval($la) % 50)) {
		header('Location: accueil.php');
		exit;
	}

	$longueur = max($lo, $la);
	$largeur = min($lo, $la);
	$_SESSION['dimensions'] = [$longueur, $largeur, $_GET['ep']];

?>
<!DOCTYPE html>
<html>
<head>
	<title>Nodex - découpez votre planche</title>
	<style type="text/css">

		html, body {
		    height: 100%;
		    margin: 0;
		}

		main.zone-dessin, section.options {
		    height: 90%;
		    padding: 1%;
		    box-sizing: border-box;
		    width: 75%;
		}

		section.options {
		    width: 24%;
		    position: absolute;
		    top: 0;
		    right: 0;
		}

		svg.support {
			width: 100%;
			height: 100%;
			background-color: #ccc;
		}

		svg.draw {
			cursor: crosshair;
		}

		.gache {
			fill: url(#gache) #715c21;
		}

		.forme {
			fill: url(#forme) #715c21;
		}
	</style>
</head>
<body>
	<main class="zone-dessin">
		<svg class="support draw" viewBox="0 0 <?= $longueur + 100; ?> <?= $largeur + 100; ?>">
			<defs> 
				<pattern id="gache" patternUnits="userSpaceOnUse" width="50" height="50"> 
					<rect width='50' height='50' fill='#fff' />
  					<path d="M10,0L0,0L0,10M10,50L0,50L0,40M40,50L50,50L50,40M50,10L50,0L40,0" style="fill: none; stroke: #683c1e; stroke-width: 5;" />
				</pattern>
				<pattern id="forme" patternUnits="userSpaceOnUse" width="50" height="50"> 
					<rect width="50" height="50" fill="#683c1e" />
					<line x1="0" y1="50" x2="50" y2="0" style="fill: none; stroke: #fff; stroke-width: 5;" /> 
					<line x1="0" y1="0" x2="50" y2="50" style="fill: none; stroke: #fff; stroke-width: 5;" /> 
				</pattern>
			</defs>
			<rect id="planche" class="gache" x="50" y="50" width="<?= $longueur; ?>" height="<?= $largeur; ?>"/>
		</svg>
	</main>
	<section class="options">
		<fieldset>
			<legend>Rectangle</legend>
			<input type="number" id="rect-x" placeholder="x" min="0" max="<?= $longueur; ?>">
			<input type="number" id="rect-y" placeholder="y" min="0" max="<?= $largeur; ?>"><br/>
			<input type="number" id="rect-width" placeholder="largeur" min="0" max="<?= $longueur; ?>">
			<input type="number" id="rect-height" placeholder="longueur" min="0" max="<?= $largeur; ?>"><br/>
			<input type="checkbox" id="rect-garde" checked="checked"/> Conserver la forme créée
			<button type="button" id="rect-add">Créer</button>
		</fieldset>
		<fieldset>
			<legend>Cercle</legend>
			<input type="number" id="circ-x" placeholder="x du centre" min="0" max="<?= $longueur; ?>">
			<input type="number" id="circ-y" placeholder="y du centre" min="0" max="<?= $largeur; ?>"><br/>
			<input type="number" id="circ-rayon" placeholder="rayon" min="0" max="<?= min($longueur, $largeur); ?>"><br/>
			<input type="checkbox" id="circ-garde" checked="checked"/> Conserver la forme créée
			<button type="button" id="circ-add">Créer</button>
		</fieldset>
		<form method="post" action="devis.php" id="formes">
			<input type="submit" value="Commander"/>
		</form>
	</section>
	<script type="text/javascript" src="scripts/snap.svg-min.js"></script>
	<script type="text/javascript" src="scripts/jquery.min.js"></script>
	<script type="text/javascript">
		var s = Snap(".support");

		var planche = s.select("#planche");

		$(document).ready(function() {
			var f = $("form#formes");
			$("#rect-add").click(function() {
				var x = parseInt($("#rect-x").val()) + 50;
				var y = parseInt($("#rect-y").val()) + 50;
				var w = $("#rect-width").val();
				var h = $("#rect-height").val();
				var garde = $("#rect-garde").is(":checked");
				var r = s.rect(x, y, w, h);
				var g = "+";
				if (garde) {
					r.attr({
						class: "forme"
					});
				} else {
					g = "-";
					r.attr({
						class: "gache"
					});
				}
				forme = g + "rect(" + [x - 50, y - 50, w, h].join(',') + ")";
				f.prepend('<input type="hidden" name="formes[]" value="' + forme + '"/>');
			});
			$("#circ-add").click(function() {
				var x = parseInt($("#circ-x").val()) + 50;
				var y = parseInt($("#circ-y").val()) + 50;
				var r = $("#circ-rayon").val();
				var garde = $("#circ-garde").is(":checked");
				var c = s.circle(x, y, r);
				var g = "+";
				if (garde) {
					c.attr({
						class: "forme"
					});
				} else {
					g = "-";
					c.attr({
						class: "gache"
					});
				}
				forme = g + "circ(" + [x - 50, y - 50, r].join(',') + ")";
				f.prepend('<input type="hidden" name="formes[]" value="' + forme + '"/>');
			})
		});
		
	</script>
</body>
</html>