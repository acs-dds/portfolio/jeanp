<?php
	session_start();
	if (isset($_SESSION['client'])) {
		unset($_SESSION['client']); // déconnexion si connecté
		$deco = true;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Nodex</title>
</head>
<body>
<h1>Bienvenue</h1>
<?php
	if (count($_GET)):
		if (isset($_GET['bien_essaye'])):
?>
<p class="error">Merci de vous identifier d'abord</p>
<?php
		elseif (isset($_GET['nope'])):
?>
<p class="error">Identifiants incorrects</p>
<?php
		endif;
	elseif (isset($deco)):
?>
<p class="info">Vous êtes déconnecté</p>
<?php endif; ?>
<form method="post" action="login.php">
	<input type="text" name="login" placeholder="Login"><br/>
	<input type="password" name="mdp" placeholder="Mot de passe">
	<input type="submit" value="Envoyer">
</form>
</body>
</html>