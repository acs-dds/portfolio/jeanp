<?php

require_once 'classes/mapper.php';
session_start();


if (!isset($_SESSION['client'])) {
	header('Location: index.php?bien_essaye');
	exit;
}


$cat = Mapper::getCatalogue($_SESSION['client']->getTypologie());
?>
<!DOCTYPE html>
<html>
<head>
	<title>Nodex - choisissez votre support</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php
		foreach ($cat as $produit):
			// echo crée implicitement un contexte string, c'est à dire que tout ce qu'on va lui donner à afficher va être converti en string
			// pour les types classiques, ça ne pose pas de problème (12 sera converti en "12", false en "0", true en "1" etc...)
			// les arrays ne sont pas convertibles en string et afficheront "Array"
			// pour les objets, la méthode magique __toString() permet de définir la façon dont on veut "stringifier" un objet
			echo $produit;
		endforeach;
	?>
	<script type="text/javascript" src="scripts/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('select.epaisseur').on('change', function() {
				var $a = $(this).siblings('a.choisir');
				var gets = $a.attr('href').split('?')[1].split('&'); // partie droite du ? dans l'url
				var page = $a.attr('href').split('?')[0]; // partie gauche du ? dans l'url
				gets[1] = 'ep=' + $(this).val(); // on modifie l'épaisseur
				$a.attr('href', page + '?' + gets.join('&')); // on recolle le tout
			});

			$('article.libre input[type=number]').on('change', function() {
				var $a = $(this).parent().siblings('a.choisir');
				var gets = $a.attr('href').split('?')[1].split('&'); // partie droite du ? dans l'url
				var page = $a.attr('href').split('?')[0]; // partie gauche du ? dans l'url
				if ($(this).attr('name') == 'lo') // longueur
					gets[2] = 'lo=' + $(this).val();
				else
					gets[3] = 'la=' + $(this).val();
				$a.attr('href', page + '?' + gets.join('&')); // on recolle le tout
			})
		});
	</script>
</body>
</html>