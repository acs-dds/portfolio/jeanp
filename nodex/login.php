<?php

session_start();

require 'classes/mapper.php';

if ($client = Mapper::getClient($_POST['login'], $_POST['mdp'])) {
		$_SESSION['client'] = $client;
		header('Location: accueil.php');
		exit;
}

header('Location: index.php?nope');
exit;