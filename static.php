<?php

class Pokemon {
	private $nom;
	private $type;

	private static $nbcrees = 0;

	public function __construct($_nom, $_type) {
		$this->nom = $_nom;
		$this->type = $_type;
		self::$nbcrees++;
	}

	public static function getNbCrees() {
		return self::$nbcrees;
	}
}

for ($i=0; $i < 10; $i++) { 
	new Pokemon('Pikachu', 'feu');
}

new Pokemon('Tortank', 'psy');

for ($i=0; $i < 3; $i++) { 
	new Pokemon('Aspicot', 'pourri');
}

echo Pokemon::getNbCrees();